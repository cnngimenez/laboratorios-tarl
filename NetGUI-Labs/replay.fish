#! /usr/bin/fish

function lab-listar-rep-grabaciones
    set -l labi "$argv[1]"
    for i in (ls -1 -d $labi/scripts/*/*.script)
        basename "$i" .script
    end 
end
function lab-listar-grabaciones
    lab-listar-rep-grabaciones "$argv[1]" | sort | uniq
end

function lab-listar-labs
    ls -d Lab*
end

function lab-start-kitty
    set -l lab "$argv[1]"
    set -l basename "$argv[2]"
    set -l divisor $argv[3]
    
    echo 'layout tall' > mykitty.session
    echo "cd $PWD" >> mykitty.session
    for file in $lab/scripts/*/$basename.script
        set -l cmd "fish replay-single.fish \"$file\" $divisor &"
        echo "launch $cmd" >> mykitty.session
    end
    kitty --session mykitty.session
end

function lab-start-xterm
    set -l lab "$argv[1]"
    set -l basename "$argv[2]"
    set -l divisor $argv[3]
    
    for file in $lab/scripts/*/$basename.script
        xterm -e "./replay-single.fish \"$file\" $divisor" &    
    end
end

function replay

    set -l lab $argv[1]
    set -l basename $argv[2]
    set -l divisor $argv[3]

    if test -z "$lab"
        lab-listar-labs
        echo "Seleccionar un Lab."
        read lab
    end
    if test -z "$basename"
        lab-listar-grabaciones "$lab"
        echo "Seleccionar un nombre de script."
        read basename
    end
    if test -z "$divisor"
          set divisor 100
    end

    ## # begin kitty
    ## # Comentar estas líneas si no se quiere utilizar Kitty.
    lab-start-kitty $lab $basename $divisor
    ## # end kitty

    ## # begin xterm
    ## # Comentar estas líneas si no se quiere utilizar xterm.
    # lab-start-xterm $lab $basename $divisor
    ## # end xterm

end
