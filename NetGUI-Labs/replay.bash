#! /bin/bash

labname=$1
scriptname=$2
divisor=$3

if test -z "$labname"; then
    echo """
Uso:

    ./replay.bash Laboratorio Script [Divisor]

- Laboratorio :: Número de laboratorio.
- Script :: Nombre de la actividad o archivo script a reproducir
  (Laboratorio/scripts/*/Script.script)
- Divisor :: Velocidad de reproducción. 1=tiempo real, 2=dos veces más 
  rápido, etc.

Ejemplos:
---------

Para reproducir lo tipeado en el laboratorio 1, actividad 1 y en tiempo
real:

    ./replay.bash Lab1 act1 1

Para reproducir lo tipeado en el laboratorio 2, actividad 1-2 (segundo 
intento) y 100 veces más rápido que el tiempo real:

    ./replay.bash Lab2 act1-2

O sino:

    ./replay.bash Lab2 act1-2 100

Este último es útil cuando solo interesa ver la salida y no el orden de
ejecución de los comandos.
"""
    exit 1
fi

if test -z "$divisor"; then
    divisor=100
    echo "Divisor por defecto: $divisor"
fi

for f in $labname/scripts/*; do
    if test -f "$f/$scriptname.script"; then
        echo "Mostrando $f/$scriptname.script"
        xterm -e "scriptreplay $f/$scriptname.script -t $f/$scriptname.tm --divisor $divisor; 
echo \"Script terminado. Presione ENTER.\";
read" &
    else
        echo "No encontrado $f/$scriptname.script"
    fi
done
