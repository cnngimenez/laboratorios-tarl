#! /bin/bash

if [ -z "${BASH_ARGV[0]}" ] ; then
    echo "Ingresar nombre de actividad (eg.: act1)."
    read scriptname
else
    scriptname="${BASH_ARGV[0]}"
fi

hostname=`hostname`
outputdir="/hostlab/scripts/$hostname"
outputbase="$outputdir/$scriptname"

# Crear directorio de scripts
if [ ! -d "$outputdir" ] ; then
    mkdir -vp "$outputdir"
fi

echo "Scripts se guardarán en $outputdir"
echo "script \"$outputbase.script\" -t 2> \"$outputbase.tm\""
echo "Presione ENTER para iniciar el volcado."
read

script "$outputbase.script" -t 2> "$outputbase.tm"
