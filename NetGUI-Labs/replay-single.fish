#! /usr/bin/fish

set -l divisor "$argv[2]"
set -l file (realpath "$argv[1]")
set -l dir (dirname "$file")
set -l tmfile $dir/(basename "$file" .script).tm

echo "scriptreplay \"$file\" -t \"$tmfile\" --divisor $divisor --maxdelay 1\""
scriptreplay "$file" -t "$tmfile" --divisor $divisor # --maxdelay 1
echo "Replay terminado. Presione ENTER para salir."
read
